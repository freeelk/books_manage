<?php

class SeedCommand extends CConsoleCommand
{
    const IMAGES_ORIG_PATH = 'images/books/originals';
    const IMAGES_THUMB_PATH = 'images/books/thumbnails';
    const IMAGES_ORIG_WIDTH = 800;
    const IMAGES_ORIG_HEIGHT = 600;
    const IMAGES_THUMB_WIDTH = 100;
    const AUTHORS_RECORDS = 10;
    const BOOKS_RECORDS = 70;

    public function run($args) {
        echo 'Seed command running' . PHP_EOL;

        $this->clearDirectory($this->getDocumentRoot() .
            self::IMAGES_ORIG_PATH . DIRECTORY_SEPARATOR . '*.*');
        $this->clearDirectory($this->getDocumentRoot() .
            self::IMAGES_THUMB_PATH . DIRECTORY_SEPARATOR . '*.*');

        $seeder = new \tebazil\yii1seeder\Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('authors')->columns([
            'id',
            'firstname'=>$faker->firstName,
            'lastname'=>$faker->lastName,
        ])->rowQuantity(self::AUTHORS_RECORDS);

        $seeder->table('books')->columns([
            'id',
            'name'=>$faker->sentence,
            'preview'=> $this->fakeImage($faker),
            'date' => $faker->date,
            'author_id' => $generator->relation('authors', 'id'),
        ])->rowQuantity(self::BOOKS_RECORDS);

        $seeder->refill();

        echo 'Make thumbnails...';
        $this->makeThumbnails();
        echo 'done';
    }

    private function fakeImage($faker) {
        $fakeImg = $faker->image( $this->getDocumentRoot() .
                       self::IMAGES_ORIG_PATH,
            self::IMAGES_ORIG_WIDTH,
            self::IMAGES_ORIG_HEIGHT,'', false);

        return $fakeImg;
    }

    private function getDocumentRoot() {
        return preg_replace('/protected$/', '', Yii::app()->basePath);
    }

    private function clearDirectory($path) {
        array_map('unlink', glob($path));
    }

    private function makeThumbnails() {
        $origImagesDir  =  $this->getDocumentRoot() . self::IMAGES_ORIG_PATH . DIRECTORY_SEPARATOR;
        $thumbsImagesDir  = $this->getDocumentRoot(). self::IMAGES_THUMB_PATH . DIRECTORY_SEPARATOR;
        foreach(glob($origImagesDir . '*.*') as $file) {
            $image = new \Eventviva\ImageResize($file);
            $image->resizeToWidth(self::IMAGES_THUMB_WIDTH);
            $image->save($thumbsImagesDir . DIRECTORY_SEPARATOR . basename($file));
        }
    }
}