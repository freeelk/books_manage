<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',

	'connectionString' => 'mysql:host=localhost;dbname=chameleonsofttest',
	'emulatePrepare' => true,
	'username' => 'loft-user',
	'password' => '123',
	'charset' => 'utf8',
);