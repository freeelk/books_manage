<?php

class BooksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'ajaxcontent', 'ajaximgsrc'),
				'users'=>array('admin', 'ajaxcontent', 'ajaximgsrc'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionAjaxcontent($id)
	{
        header('Content-type: application/json');

		$model = $this->loadModel($id);
        $data = [];
        $data['id'] = $model->id;
        $data['name'] = $model->name;
        $data['preview'] = Yii::app()->baseUrl . "/images/books/thumbnails/" . $model->preview;
        $data['author'] = Authors::model()->findByPk($model->author_id)->author;
        $data['date'] = $model->date;
        $data['date_create'] = $model->date_create;

        echo CJSON::encode($data);
	}

    public function actionAjaximgsrc($id)
    {
        header('Content-type: application/json');
        $model = $this->loadModel($id);
        $imgSrc = array('src'=> Yii::app()->baseUrl ."/images/books/originals/" .
                                $model->preview,
                        	'name' => $model->name,);
        echo CJSON::encode($imgSrc);
    }


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Books;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Books']))
		{
			$model->attributes=$_POST['Books'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Books']))
		{
			$_POST['Books']['preview_img'] = $model->preview_img;
			$model->attributes=$_POST['Books'];

			$uploadedFile=CUploadedFile::getInstance($model,'preview_img');


			if($model->save())
				if(!is_null($uploadedFile))  // check if uploaded file is set or not
				{
					$uploadedFile->saveAs(Yii::app()->basePath.'/../images/books/originals/'.$model->preview);
					$image = new \Eventviva\ImageResize(Yii::app()->basePath.'/../images/books/originals/'.$model->preview);
					$image->resizeToWidth(100);
					$image->save(Yii::app()->basePath.'/../images/books/thumbnails/'.$model->preview);

				}
				$this->redirect(array('admin', 'loadSPS' => 1));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Books');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		if (isset($_GET['loadSPS'])) {
            if (!isset($_GET['loadSPS1'])) {
                self::initSearchPageSort('Books', 'load');
            } else {
                unset($_GET['loadSPS1']);
            }
			unset($_GET['loadSPS']);
		}

		self::initSearchPageSort('Books');

		unset(Yii::app()->request->cookies['datefrom']);
		unset(Yii::app()->request->cookies['dateto']);

		$model=new Books('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['datefrom']) && isset($_GET['dateto']))
		{

			Yii::app()->request->cookies['datefrom'] = new CHttpCookie('datefrom', $_GET['datefrom']);
			Yii::app()->request->cookies['dateto'] = new CHttpCookie('dateto', $_GET['dateto']);
			$model->datefrom = $_GET['datefrom'];
			$model->dateto = $_GET['dateto'];
		}

		if(isset($_GET['Books']))
			$model->attributes=$_GET['Books'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Books the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Books::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Books $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='books-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    //Explanation:  http://www.yiiframework.ru/forum/viewtopic.php?t=3630
	protected static function initSearchPageSort($modelName, $action = 'save') {
		/** @var WebUser $webUser */
		$webUser = Yii::app()->user;
		$params = array(
			"{$modelName}",
			"{$modelName}_page",
			"{$modelName}_sort",
		);
		foreach ($params as $p) {
			$_p = "_{$p}";
			switch ($action) {
				case 'save' :
					if (isset($_GET[$p])) {
						$webUser->setState($_p, $_GET[$p]);
					} else {
						$webUser->setState($_p, null);
					}
					break;
				case 'load' :
					$data = $webUser->getState($_p);
					if ( isset($data) ) {
						$_GET[$p] = $data;
					}
					break;
				case 'reset' :
					$webUser->setState($_p, null);
					break;
			}
		}
	}

}
