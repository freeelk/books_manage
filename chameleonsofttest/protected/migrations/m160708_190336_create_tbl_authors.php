<?php

class m160708_190336_create_tbl_authors extends CDbMigration
{
	public function up()
	{
		$this->createTable('authors', array(
			'id' => 'pk',
			'firstname' => 'VARCHAR(50) NOT NULL',
			'lastname' => 'VARCHAR(50) NOT NULL',
		) , 'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
        $this->dropTable('authors');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}