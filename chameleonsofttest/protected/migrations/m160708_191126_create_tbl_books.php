<?php

class m160708_191126_create_tbl_books extends CDbMigration
{
	public function up()
	{

		$this->createTable('books', array(
			'id' => 'pk',
			'name' => 'varchar(255) NOT NULL',
			'date_create' => 'timestamp DEFAULT \'0000-00-00 00:00:00\'',
			'date_update' => 'timestamp DEFAULT now() on update now()',
			'preview' => 'varchar(255) NOT NULL',
			'date' => 'date NOT NULL',
			'author_id' => 'int NOT NULL',
		) , 'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
        $this->dropTable('books');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}