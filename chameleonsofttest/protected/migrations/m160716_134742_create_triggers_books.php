<?php

class m160716_134742_create_triggers_books extends CDbMigration
{
	public function up()
	{
		$createTriggerBooksCreatedSql = <<< SQL
CREATE TRIGGER books_created BEFORE INSERT ON books
FOR EACH ROW BEGIN
SET new.date_create := now();
SET new.date_update := now();
END;
SQL;

		$createTriggerBooksUpdatedSql = <<< SQL
CREATE TRIGGER books_updated BEFORE UPDATE ON books
FOR EACH ROW BEGIN
SET new.date_update := now();
END;
SQL;

		$this->execute($createTriggerBooksCreatedSql);
		$this->execute($createTriggerBooksUpdatedSql);
	}

	public function down()
	{
		$this->execute('DROP TRIGGER /*!50032 IF EXISTS */ `books_created`');
		$this->execute('DROP TRIGGER /*!50032 IF EXISTS */ `books_updated`');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}