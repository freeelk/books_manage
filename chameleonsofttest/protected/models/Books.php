<?php

/**
 * This is the model class for table "books".
 *
 * The followings are the available columns in table 'books':
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 */
class Books extends CActiveRecord
{

	public $author;
	public $datefrom;
	public $dateto;
	public $preview_img;
    public $loadSPS1;

	public function getAuthor()
	{
		return $this->authors->firstName . ' ' . $this->authors->lastName;
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'books';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, date_update, preview, date, author_id', 'required'),
			array('author_id', 'numerical', 'integerOnly'=>true),
			array('name, preview', 'length', 'max'=>255),
			array('date_create', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, date_create, date_update, preview, date, author_id, author, datefrom, dateto', 'safe', 'on'=>'search'),
			array('preview_img', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			array('preview_img', 'length', 'max'=>255, 'on'=>'insert,update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'authors' => array(self::BELONGS_TO, 'Authors', 'author_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'date_create' => 'Дата добавления',
			'date_update' => 'Дата обновления',
			'preview' => 'Превью',
			'date' => 'Дата выхода книги',
			'author_id' => 'Author ID',
			'author' => 'Автор',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$sort = new CSort;

		$criteria->with = array(
			'authors' => array()
		);

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('firstname, lastname', $this->author, true);


		if (!empty($this->datefrom) && empty($this->dateto)) {
			$criteria->addCondition("date >= '{$this->datefrom}'");
		} elseif(!empty($this->dateto) && empty($this->dateto)) {
			$criteria->addCondition("date <= '{$this->dateto}'");
		} elseif(!empty($this->dateto) && !empty($this->dateto)) {
			$criteria->addCondition(
				"date  >= '{$this->datefrom}' and date <= '{$this->dateto}'");
		}

		$sort->sortVar = get_class($this) . '_sort';
		$sort->attributes = array(
			'author'=>array(
				'asc'=>'firstname, lastname',
				'desc'=>'firstname desc, lastname desc',
			),
			'*',
		);

		return new CActiveDataProvider($this, array(
			'pagination'=>array('pageSize'=>10),
			'criteria'=>$criteria,
			'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Books the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
