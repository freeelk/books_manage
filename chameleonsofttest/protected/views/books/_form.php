<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'books-form',

	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'preview'); ?>
		<?php echo CHtml::activeFileField($model, 'preview_img'); ?>
		<?php echo $form->error($model,'preview'); ?>
	</div>
	<?php if($model->isNewRecord!='1'){ ?>
	<div class="row">
		<?php echo CHtml::image(Yii::app()->request->baseUrl.
			'/images/books/thumbnails/'.$model->preview,"preview",array("max-width"=>'100px')); }?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'date'); ?>

		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model'=>$model,
			'name'=>'date',
			'attribute' => 'date',
            'options'=>array(
                'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
            ),
        ));
		?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
        <?php echo $form->dropDownList($model,'author_id',
            CHtml::listData(Authors::model()->findAll(array('order' => 'firstname, lastname')),'id','author'),
            array('empty'=>'--Автор--'));
        ?>
		<?php echo $form->error($model,'author_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->