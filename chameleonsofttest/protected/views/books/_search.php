<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'enableAjaxValidation'=>true,
));


CHtml::hiddenField('loadSPS1' , '1', array('id' => 'hiddenInput'));
//$form->hiddenField($model,'loadSPS1', array('value'=>'1'));

?>

    <input type="hidden" name="loadSPS1" value="1"/>

	<div class="row">
        <?php echo $form->dropDownList($model,'author_id',
                   CHtml::listData(Authors::model()->findAll(array('order' => 'firstname, lastname')),'id','author'),
                  array('empty'=>'--Автор--'));
        ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255, 'placeholder'=>'название книги')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>

		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'datefrom',
		'value'=> isset(Yii::app()->request->cookies['datefrom']) ? Yii::app()->request->cookies['datefrom']->value : '',
		'language'=>'ru',
		'options'=>array(
		'showAnim'=>'fold',
		'dateFormat'=>'yy-mm-dd',
		),
		'htmlOptions'=>array(
			'style'=>'height:25px;'
		),
		));
		?>
		до
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=>'dateto',
			'value'=>isset(Yii::app()->request->cookies['dateto']) ? Yii::app()->request->cookies['dateto']->value : '',
			'language'=>'ru',
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>'yy-mm-dd',
			),
			'htmlOptions'=>array(
				'style'=>'height:25px;'
			),
		));
		?>
		<div style="float: right;">
		<?php echo CHtml::submitButton('Искать'); ?>
		</div>	
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->