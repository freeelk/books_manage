<?php
/* @var $this BooksController */
/* @var $model Books */

$this->breadcrumbs = array(
    'Books' => array('index'),
    'Manage',
);

/*$this->menu = array(
    array('label' => 'List Books', 'url' => array('index')),
    array('label' => 'Create Books', 'url' => array('create')),
);*/

Yii::app()->clientScript->registerScript(
    'search',
    "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#books-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
"
);
?>

<h1>Управление списком книг</h1>

<!--<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>,
    <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how
    the comparison should be done.
</p>
-->

<?php //echo CHtml::link(
    //'Advanced Search',
    //'#',
    //array('class' => 'search-button')
//); ?>
<!--<div class="search-form" style="display:none">-->
    <div class="search-form" >
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php
$image
    = 'CHtml::image(Yii::app()->baseUrl ."/images/books/thumbnails/" .$data->preview,"",array(\'max_width\'=>\'100\'))';
$imageLink
    = 'CHtml::ajaxLink(
    '.$image.',
    array(\'books/ajaximgsrc\', \'id\' => $data->primaryKey ),
    array(
        \'type\' => \'POST\',
        \'data\'=>array(\'update\'=>TRUE),
        \'success\' => \'function(data){
            $("#previewModal").modal("show");
            $("#preview-image").attr("src",data.src);
            $("#preview-image-descr").html(data.name);
        }\',
    ))';

$this->widget(
    'zii.widgets.grid.CGridView',
    array(
        'id'           => 'books-grid',
        'dataProvider' => $model->search(),
        'ajaxUpdate'   => false,
        'columns'      => array(
            'id',
            'name',
            array(
                'labelExpression' => $imageLink,
                'header'          => 'Превью',
                'class'           => 'CLinkColumn',
            ),
            array(
                'name'   => 'author',
                'header' => CHtml::encode($model->getAttributeLabel('author')),
                'value'  => '$data->authors->author',
                'filter'=> CHtml::activeTextField($model, 'author'),
            ),
            array(
                'name'=>'date',
                'value' => 'Yii::app()->dateFormatter->format(\'d MMMM yyyy\', $data->date)',
            ),
            array(
              'name'=>'date_create',
              'value' => 'Yii::app()->dateFormatter->format(\'d MMMM yyyy\', $data->date_create)',
            ),
            array(
                'header'=>'Кнопки действий',
                'class'                => 'CButtonColumn',
                'template'             => '{view} {update} {delete}',
                'deleteButtonImageUrl' => false,
                'updateButtonImageUrl' => false,
                'viewButtonImageUrl'   => false,
                'buttons'              => array
                (
                    'update' => array
                    (
                        'label' => '[ред]',
                    ),
                    'view'   => array
                    (
                        'label' => '[просм]',
                        'url'   => 'Yii::app()->createUrl("books/ajaxcontent", array("id"=>$data["id"]))',
                        'click' => "function(){ $.ajax({
                                type:'POST',
                                cache:false,
                                url  : $(this).attr('href'),
                                success: function(data) {
                                  $('#detailViewModal').modal('show');
                                  $('#detail-view-id').html(data.id);
                                  $('#detail-view-name').html(data.name);
                                  $('#detail-view-image').attr('src',data.preview);
                                  $('#detail-view-author').html(data.author);
                                  $('#detail-view-date').html(data.date);
                                  $('#detail-view-date-create').html(data.date_create);
                                },
                                });
                              return false;}",
                    ),
                    'delete' => array
                    (
                        'label' => '[удл]',
                    ),

                ),
            ),
        ),
    )
);
?>

<?php $this->widget(
    'bootstrap.widgets.TbModal',
    array(
        'id'      => 'previewModal',
        'header'  => 'Превью',
        'content' => '<div><img id="preview-image" style="max-width: 100%;" src=""></div><div id="preview-image-descr">TEST</div>',
        'footer'  => array(
            TbHtml::button('Close', array('data-dismiss' => 'modal')),
        ),
    )
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbModal',
    array(
        'id'      => 'detailViewModal',
        'header'  => 'Детальный просмотр',
        'content' => '<div id="detail-view-content">
<table class="table table-striped">
  <tbody>
    <tr>
      <th scope="row">ID</th>
      <td><div id="detail-view-id"></div></td>
    </tr>
    <tr>
      <th scope="row">Название</th>
      <td><div id="detail-view-name"></div></td>
    </tr>
    <tr>
      <th scope="row">Превью</th>
      <td><img id="detail-view-image" style="max-width: 100px;" src=""/></td>
    </tr>
    <tr>
      <th scope="row">Автор</th>
      <td><div id="detail-view-author"></div></td>
    </tr>
    <tr>
      <th scope="row">Дата выхода книги</th>
      <td><div id="detail-view-date"></div></td>
    </tr>
    <tr>
      <th scope="row">Дата добавления</th>
      <td><div id="detail-view-date-create"></div></td>
    </tr>
  </tbody>
</table></div>',
        'footer'  => array(
            TbHtml::button('Close', array('data-dismiss' => 'modal')),
        ),
    )
); ?>